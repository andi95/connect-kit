#include <fcntl.h>
#include <string.h>
#include "../Unity/src/unity.h"
#include "../src/connectkit.h"
#include "uci.h"

struct uci_context* myconfig_ctx;

void setUp(){

    myconfig_ctx = uci_alloc_context();
    uci_set_confdir(myconfig_ctx, PATH);

}

void test_Return_number_config_files(void)
{
    char **configs = NULL;
    TEST_ASSERT_EQUAL(10, count_uci_configs(myconfig_ctx, configs));
}

void test_Non_null_config_package_loader(void)
{
    char cmd [] = "network";
    FILE * input = uci_open_stream(myconfig_ctx, cmd, 1 , O_RDONLY,O_CREAT);
    struct uci_package * package = load_config(myconfig_ctx, cmd, input);
    TEST_ASSERT_EQUAL(0,strcmp(cmd, package->e.name));
}

/*Inzio i test*/
int main(void)
{
    UNITY_BEGIN();
    RUN_TEST(test_Non_null_config_package_loader);
//    RUN_TEST(test_Return_number_config_files);
    return UNITY_END();
}