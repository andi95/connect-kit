#include "connectkit.h"
#include <sys/types.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/file.h>
#include <sys/stat.h>
#include <errno.h>

struct uci_element *Get_ip_address(const char *ctx) {
    return NULL;
}

bool Set_ip_address(struct uci_context *ctx, struct uci_ptr *ptr) {
    return false;
}


struct uci_package *load_config(struct uci_context *ctx, char *package_name, FILE* input) {
    struct uci_package *package = NULL;
    if (uci_load(ctx, package_name, &package) != UCI_OK)
        uci_perror(ctx, "Error in package loader");
    if (uci_import(ctx, input, package_name, &package, (package_name != NULL)) != UCI_OK)
        uci_perror(ctx, "Error in package import");
    return package;
}

int count_uci_configs(struct uci_context *ctx, char **list) {
    int counter = -1; // Use this as flag success and return number
    char **p;
    if ((uci_list_configs(ctx, &list) != UCI_OK) || !&list) {
        uci_perror(ctx, "Error in config number loader");
        goto out;
    }
    for (p = list; *p; p++) {
        DEBUG_PRINT(("%s\n", *p));
        counter++;
    }
    out:
    free(list);
    return counter;
}

FILE *uci_open_stream(struct uci_context *ctx, const char *filename, int pos, bool write, bool create)
{
    struct stat statbuf;
    FILE *file = NULL;
    int fd, ret;
    int mode = (write ? O_RDWR : O_RDONLY);

    if (create)
        mode |= O_CREAT;

    if (!write && ((stat(filename, &statbuf) < 0) ||
                   ((statbuf.st_mode &  S_IFMT) != S_IFREG))) {
        printf("Error file not found %i", UCI_ERR_NOTFOUND);
    }

    fd = open(filename, mode, UCI_FILEMODE);
    if (fd < 0)
        goto error;

    ret = flock(fd, (write ? LOCK_EX : LOCK_SH));
    if ((ret < 0) && (errno != ENOSYS))
        goto error;

    ret = lseek(fd, 0, pos);

    if (ret < 0)
        goto error;

    file = fdopen(fd, (write ? "w+" : "r"));
    if (file)
        goto done;

    error:
    printf("Error opening file: %i", UCI_ERR_IO);
    done:
    return file;
}