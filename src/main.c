#include <ctype.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include "connectkit.h"


int
main (int argc, char **argv)
{
    int aflag = 0;
    int bflag = 0;
    char *cvalue = NULL;
    int index;
    int c;

    opterr = 0;

    DEBUG_PRINT(("path: %s,\n", PATH));

    while ((c = getopt (argc, argv, "angd:")) != -1)
        switch (c)
        {
            case 'a':
                aflag = 1;
                break;
            case 'n':
                break;
            case 'g':
                break;
            case '?':
                if (optopt == 'c')
                    fprintf (stderr, "Option -%c requires a debug message.\n", optopt);
                else if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,
                             "Unknown option character `\\x%x'.\n",
                             optopt);
                return 1;
            default:
                _exit (1);
        }


    printf ("aflag = %d, bflag = %d, cvalue = %s\n",
            aflag, bflag, cvalue);

    for (index = optind; index < argc; index++)
        printf ("Non-option argument %s\n", argv[index]);

    struct uci_context* myconfig_ctx;
    myconfig_ctx = uci_alloc_context();
    uci_set_confdir(myconfig_ctx, PATH);
    char cmd [] = "dhcp";
    FILE * input = uci_open_stream(myconfig_ctx, cmd, 1 , O_RDONLY, O_CREAT);
    load_config(myconfig_ctx, cmd, input);
    struct uci_package * package = load_config(myconfig_ctx, cmd, input);
    printf("%s", package->e.name);
    return 0;
}
