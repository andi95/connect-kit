/**
*   @mainpage Connect Kit
*
*   @author Andi Dulla
*
*   @date 24 Jul 2018
*
*   Everything is accomplished using the \b libuci interface, which is the UCI C library of the OpenWRT system.
*   The functions implemented with libuci will closely resemble the command line usage. Example below:
*   \code
*   uci show network
*   network.loopback=interface
*   network.loopback.ifname='lo'
*   network.loopback.proto='static'
*   \endcode
*
* This is the correspondence between UCI command line and C syntax and mode:
*      \arg @b context : struct uci_context* gets everything ready (I think)
*      \arg @b packet : struct uci_package* corresponds to a file in UCI format
*      \arg @b section : struct uci_list* it's a configuration node
*      \arg @b value : generic value type dependent (struct uci_ptr)
*
* @see https://git.openwrt.org/project/uci.git
* @see https://wiki.openwrt.org/doc/techref/uci
*/


 /** @file connectkit.h
 *  @brief Header to be included
 *
 *  @see http://technostuff.blogspot.com/2017/03/openwrt-modules-uci.html
 *  @see http://forgotfun.org/2016/02/%E9%87%8D%E6%96%B0%E5%B0%81%E8%A3%85OpenWRT%E4%B8%8B%E7%9A%84LibUCI%E5%BA%93-%E9%99%8D%E4%BD%8E%E4%BD%BF%E7%94%A8%E9%9A%BE%E5%BA%A6.html
 *  This file contains all of the function definitions (moslty wrappers) in order to configure the system.
 */



#ifndef CONNECTKIT_CONNECTKIT_H
#define CONNECTKIT_CONNECTKIT_H


#ifndef DEBUG
    #define DEBUG
#endif

#ifdef DEBUG
    # define DEBUG_PRINT(x) printf x
#else
    # define DEBUG_PRINT(x) do {} while (0)
#endif

#ifdef DEBUG
    #define PATH "/home/andi/Desktop/ConnectKit/mock"
#else
    #define PATH "/etc/config"
#endif

#include <stdbool.h>
#include <uci.h>

/** @brief IP address setter for a specific interface
 *
 *  This is the setter of the interface address, which will return a bool indicating if there was an error.
 *
 *  @todo Add the error indication by returning a "struct error" and so on...
 *
 *  @param ctx The context in order to tell the system to configure
 *  @param ptr The struct_ptr that holds all of the informations needed
 *  @return bool success/failure
 */
bool
Set_ip_address(struct uci_context *, struct uci_ptr *);

/** @brief IP address getter for a specific interface
 *
 *  This is the getter of the interface address, which will return a struct uci_element and not a struct sockaddr. We
 *  can printf the whole content by using the foreach function.
 *
 *  @param ctx The context in order to tell the system to configure
 *  @param ptr The struct_ptr that holds all of the informations needed
 *  @return uci_element struct containig the desired info.
 */
struct uci_element*
Get_ip_address(const char *);

/** @brief Configuration loader
 *
 *  We need this funciton in order to get a runnig instance of the context which will be used by the system to populate
 *  all the info that will be fetched or setted from the user.
 *
 *  @param path Path of the config folder (MOCK for now)
 *  @return uci_element struct containig the desired info.
 */

struct uci_package *
load_config(struct uci_context *,char *, FILE* );

/** @brief Configuration printer and number getter
 *
 *  We use this funciton to get te number of the config files available in the system.
 *
 *  @note The number of the config files is decremented by one! Be careful of @b int implementation (platform dependent)
 *
 *  @param ctx The context in order to tell the system to configure
 *  @param path triple pointer which holds the list, will be configured by libuci
 *  @return -1 on failure and integer on success
 */
int
count_uci_configs(struct uci_context *ctx, char **list);

FILE *uci_open_stream(struct uci_context *ctx, const char *filename, int pos, bool write, bool create);


#endif //CONNECTKIT_CONNECTKIT_H
